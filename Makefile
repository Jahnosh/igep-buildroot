BUILDIR		= $(CURDIR)/build
SRCDIR		= $(CURDIR)/src
BR_EXT 		= $(CURDIR)/buildroot-external
BR 			= $(CURDIR)/buildroot
BOARD		= IGEPv2

.DEFAULT_GOAL := build
.PHONY: setup config source build menuconfig

setup:
	git submodule update --init

config: $(BUILDIR)/.config

$(SRCDIR) $(BUILDIR):
	mkdir -p $@

source: config
	$(MAKE) -C $(BUILDIR) BR2_DL_DIR=$(SRCDIR) source

build: source
	$(MAKE) -C $(BUILDIR) BR2_DL_DIR=$(SRCDIR)

$(BUILDIR)/.config: $(BR_EXT)/configs/$(BOARD)_defconfig $(BR)/Makefile
	mkdir -p $(BUILDIR)
	$(MAKE) O=$(BUILDIR) -C $(BR) BR2_EXTERNAL=$(BR_EXT) BR2_DL_DIR=$(SRCDIR) $(BOARD)_defconfig

$(BR)/Makefile:
	$(MAKE) setup

menuconfig: config
	$(MAKE) -C $(BUILDIR) menuconfig

savedefconfig: config
	$(MAKE) -C $(BUILDIR) savedefconfig BR2_DEFCONFIG=$(BR_EXT)/configs/$(BOARD)_defconfig

clean:
	$(MAKE) -C $(BUILDIR) clean

distclean:
	rm -rf $(BUILDIR)
